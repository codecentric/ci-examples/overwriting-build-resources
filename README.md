# overwriting-build-resources

Dieses Projekt zeigt wie mittels Environment variablen an einem build job die default Werte für die verfügbare CPU und den RAM überschrieben werden können.

Siehe: https://docs.gitlab.com/runner/executors/kubernetes.html#overwriting-build-resources
```
   KUBERNETES_CPU_REQUEST: 1
   KUBERNETES_CPU_LIMIT: 1
   KUBERNETES_MEMORY_REQUEST: 2Gi
   KUBERNETES_MEMORY_LIMIT: 2Gi
```
